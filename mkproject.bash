#!/bin/bash

set -x

if [[ $# -ge 1 ]]; then
	mkdir -p "$1"
    cd "$1"
fi

mkdir -p static
mkdir -p static/css
mkdir -p static/image
mkdir -p static/js
mkdir -p static/pluigins

mkdir -p templates

cat << EOF > templates/footer.php
<?php

// footer

?>
EOF

cat << EOF > templates/header.php
<?php

// header

?>
EOF

cat << EOF > templates/source.php
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta property="og:description" content="" />
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function() {
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#edeff5",
                        "text": "#838391"
                    },
                    "button": {
                        "background": "transparent",
                        "text": "#4b81e8",
                        "border": "#4b81e8"
                    }
                }
            })
        });
    </script>
    <title>Basic Site - <?php echo \$title ?></title>
</head>
EOF

cat << EOF > templates/scripts.php
<?php

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

?>
EOF

cat << EOF > index.php
<!doctype html>
<html lang="en" class="">
<?php \$title = ''; require 'templates/source.php'; ?>

<body>

</body>

</html>
EOF

composer require "sentry/sentry"